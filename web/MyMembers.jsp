<%-- 
    Document   : MyMembers
    Created on : 7 déc. 2017, 13:16:11
    Author     : Mohammed Mehdi Sarray#
--%>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>

<%@ page import="java.util.*" %>
<%@page import="java.util.Iterator" %>
<%@page import="java.util.List"%>
<%@page import="dao.beans.membre" %>
<%@page import="dao.daomembre" %>

<!DOCTYPE html>
<html>
     <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MEDIA-VOTE</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css2/shop-item.css" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
   <%@include file="nav_Admin.jsp" %>

    <!-- Page Content -->
    <div class="container">

      <div class="row">

        
        <!-- /.col-lg-3 -->

        <div class="col-lg-9" id="contenu">
         

         <%  
            
              List<Map<String, String>> maListe = new ArrayList<Map<String, String>>();
                daomembre modetud= new  daomembre();
         maListe = modetud.lister_membre();
       
                  // if (maListe != null) {
                       for (Map<String, String> entry : maListe) {
                                    
                      
                         %> 
                       
                     
                      Nom :  <h3 class="card-title"><%= entry.get("nom") %></h3>  
                      Prenom :   <p class="card-text"><%= entry.get("prenom") %></p>
                      Cin :   <p class="card-text"><%= entry.get("cin") %></p>
		      Email :<p class="card-text" >   <%= entry.get("email") %> <br> <p/> 
                      Type :   <p class="card-text" >   <%= entry.get("type") %> <br> <p/> 
                              
                         <span>
                          <input type="button" id="submit" value="Supprimer" onclick="window.location.href='Supprimer?action=suppmembre&id=<%= entry.get("login")  %>'" />
                         
                         </span>
                         
                         
                          <br/>
         
                       <%        
                             }
                        
                           // }
                   
                         %>
                             
              </div>
            
          <!-- /.card -->

          <div class="card card-outline-secondary my-4">
            
          </div>
          <!-- /.card -->

        </div>
        <!-- /.col-lg-9 -->

      </div>

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Media-Vote 2017</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper/popper.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script>
       
</script>

    </body>
</html>


